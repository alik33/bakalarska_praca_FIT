Thesis description:
	Data of patients who undergo allogeneic hematopoietic transplantation in Institute of Hematology and Blood Transfusion (Ústav hematologie a krevní transfuze - ÚHKT) are stored in MySQL database. Interface for retrieving these data is written in PHP and this interface runs over ÚHKT intranet.

	Within the scope of this thesis is to design and implement an interactive web application in programming language R with help of its framework Shiny (shiny.rstudio.com). The practical use of this application is to analyse and visualize data stored in the database. Its outputs will be used for quality monitoring and scientific purposes.

Abstrakt:
	Cieľom tejto práce je vytvorenie interaktívnej webovej aplikácie. Venuje sa analýze a vizualizácii dát o pacientoch, ktorí podstupujú alogénnu transplantáciu krvotvorby. Vizualizácie a analýza sú rozdelené do 4 kategórií a každá z nich sa zaoberá rozdielnou problematikou. Práca detailnejšie popisuje, ako jednotlivé dôležité štatistické funkcie, ktoré sú v aplikácii využívané, fungujú. Aplikácia poskytuje uživateľské rozhranie a serverovú logiku, ktoré sú napísané v jazyku R za pomoci frameworku Shiny. Spracovávané dáta sú zhromažďované v databázi MySQL. Táto aplikácia má slúžiť pre kontrolu kvality vykonávaných úkonov v ÚHKT a pre vedecké účely.
kľúčové slová: transplantácia krvotvorby, R Shiny, štatistika, dátová analýza

Abstract (EN):
	The goal of this thesis is to create an interactive web application. The application analyzes and visualizes patient data. These patients undergo allogeneic hematopoietic transplantation. Visualizations and analysis are divided into 4 different categories and each category follows up different problems. Thesis describes in detail how individual statistical functions which are used in the application work. The application provides user interface and server logic. Both of these are written in programming language R with help of its framework Shiny. Processed data are collected and stored in MySQL database. This application will serve ÚHKT personnel for quality monitoring and it will be also used for scientific purposes.


Úvod:
	Transplantácie krvotvorby sa robia v ÚHKT najdlhšiu dobu v celej Českej republike a taktiež aj v najväčšom počte. Za rok ich lekári z tohto ústavu spravia niekoľko desiatok a spolu ich tu vykonali niečo cez tísíc. Ich metódy sa stále zlepšujú a mojou prácou mám k tomu istým spôsobom prispieť. O každom pacientovi, ktorý podstúpi transplantáciu krvotvorby, sa ukladajú určité dáta. Mnou spravená aplikácia má práve na týchto zbieraných dátach robiť potrebné štatistiky a analyzovať ich. Keď človek vidí surové dáta, tak z nich nevie veľa vyčítať. Surové data nebývajú často v takom stave, aby sa s nimi dalo vhodne narábať. Existujú rôzne problémy, ktoré nám sťažujú manipulovanie s dátami, no, našťastie, dáta, s ktorými bude pracovať moja aplikácia sú vo veľmi dobrom stave a nie je potrebné ich žiadnym spôsobom čistiť. Štatistiky, ktoré moja webová aplikácia vykonáva, sú rozdelené do 4 kategórií. Každá z týchto kategórií analyzuje inú problematiku. Prvá kategória je nazvaná "Trendy v čase". V tejto kategórii rozoberám vývoj atribútov môjho data setu v čase. Prevažne sa jedná o problém zobrazenia počtov jednotlivých atribútov v určitom časovom období.

Introduction:
	ÚHKT is known for performing hematopoietic transplantations for the longest period of time in whole Czech Republic. They also do the most transplantations out of all the institutes in Czech Republic in all periods of time. Doctors from this institute perform multiple tens of these transplantations in one year and in total they performed more than one thousand of these transplantations. Their methods are still getting better and my piece of work should contribute to improvement of these methods. Data of each patient who undergo hematopoietic transplantation are stored in their database. The purpose of my application is to analyze these stored data and create statistics above these data in real time.
	When a person sees raw data, many times he can not say much about them. Many times, if not most of the times, raw data are not in appropriate form. They are not adjusted for further manipulation. There exist different kinds of problems of raw data but, fortunately, these data which my application analyzes have very good shape and it in and in most of the times it is not neccessary to anyhow clean these data. Statistics and analyzes created by my application are divided into 4 categories. Each one of these categories deals with different problem(s).
	1st category is named Trends in time. In this category I examine evolution of attributes of my data in time. Mostly this is a matter of visualizing attributes in specific time period.
	2nd category is named Frequency tables. Depending on what variables (attributes) user chooses, the table containing all combinations of these variables and their frequencies is displayed to user.
	3rd category is named ****Stepy a prihojenie****. ****TREBA DOPISAT****
	4th category is named Survival analysis. This one plots curves to graphs. These curves analyze survival in time. For each x value there is one responding y value for each curve. This y value says how many of patients survived for period of time (x value).

Realisation:
	- at first I was using .csv file with testing data, then I switched data source from .csv to MySQL database - complications occured so I was forced to fix them - At first, I was testing every graph manually against .csv file. Then I tested all the graphs again but now against MySQL database.

Citace:
	https://www.uhkt.cz/pacient/transplantace-kostni-drene
	https://cs.wikipedia.org/wiki/RStudio
	https://shiny.rstudio.com/
	https://www.cscu.cornell.edu/news/statnews/stnews78.pdf
	https://www.youtube.com/watch?v=1Fz6kBXmAt0
	https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3059453/ - kaplan meier explained