CVUT FIT bachelor's thesis

## Title
  
Interactive application to analyze clinical data of patients after transplantation of hematopoiesis.

## Thesis description

Data of patients who undergo allogeneic hematopoietic transplantation 
in Institute of Hematology and Blood Transfusion (Ústav hematologie a krevní transfuze - ÚHKT) are stored in MySQL database. 
Interface for retrieving these data is written in PHP and this interface runs over ÚHKT intranet.

Within the scope of this thesis is to design and implement an interactive web application 
in programming language R with help of its framework Shiny (shiny.rstudio.com). 
The practical use of this application is to analyse and visualize data stored in the database. 
Its outputs will be used for quality monitoring and scientific purposes.

## Running the application

Run the application with the following lines of code:
```R
library(shiny)  # load shiny library
application_folder <- "C:/Users/Test/app/"  # change to your application directory
app_port <- 8080  # change to whatever free port - app will be listening on this port
runApp(application_folder, app_port)
```