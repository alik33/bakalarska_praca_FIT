# Graph number of transplantions in years
# data_inmemory_tab1 <- 
#   data_tab1 %>% select(rok_transplantace, dg_group)

# Graph 1
output$transplantace_v_letech <- renderPlot(
  {
    plot_data <- data.frame(data_tab1 %>% select(rok_transplantace))
    plot_data$rok_transplantace <- as.factor(plot_data$rok_transplantace)
    ggplot(plot_data, 
           aes(rok_transplantace)) +
      geom_bar() +
      labs(x = "Rok transplantace", y = "Počet diagnóz")
  }
)
#-------------------------------------------------------------------------------
# Graph 1.3 - transplants by diagnosis
output$transplantace_podle_diagnoz_input_panel <- renderUI(
  wellPanel(
  selectInput("year", "Rok:",
              choices = c(none_year_selected, unique_transplant_years_decreasing),
              width = "100%"),
  class = 'vertical-layout-well-panel-input'
  )
)
output$transplantace_podle_diagnoz <- renderPlot(
  {
    if(is.null(input$year))
      return()
    ggplot(
      if(input$year == none_year_selected)
        data.frame(data_tab1 %>% select(dg_group))
      else
        data.frame(data_tab1 %>% filter(rok_transplantace == input$year) %>% select(dg_group)),
      aes(dg_group)) +
      geom_bar() +
      coord_flip() +
      labs(x = "Diagnostická skupina", y = "Počet")
  }
)
#-------------------------------------------------------------------------------
# Graph 1.4 - spectrum of diagnosis in time
output$vyvoj_spektra_diagnoz_input_panel <- renderUI(
  wellPanel(
    sliderInput("vyvoj_spektra_diagnoz_input_years", "Roky:",
                min = unique_transplant_years_decreasing[length(unique_transplant_years_decreasing)],
                max = unique_transplant_years_decreasing[1],
                value = c(unique_transplant_years_decreasing[1] - 5,
                          unique_transplant_years_decreasing[1]),
                step = 1,
                sep = '',
                width = '100%'),
    class = 'vertical-layout-well-panel-input'
  )
)
output$vyvoj_spektra_diagnoz <- renderPlot(
  {
    input_year_bot <- input$vyvoj_spektra_diagnoz_input_years[1]
    input_year_top <- input$vyvoj_spektra_diagnoz_input_years[2]
    if(any(sapply(list(input_year_bot,
                       input_year_bot), is.null)
    ) == TRUE)
      return()
    
    years_seq <- seq(input$vyvoj_spektra_diagnoz_input_years[1],
                     input$vyvoj_spektra_diagnoz_input_years[2])
    plot_data <- data.frame(data_tab1 %>% 
                              filter(rok_transplantace %in%
                                years_seq) %>% 
                              select(dg_group, rok_transplantace))
    ggplot(
      plot_data,
      aes(dg_group)
      ) +
      geom_bar() +
      facet_grid(. ~ rok_transplantace) +
      coord_flip() +
      labs(x = "", y = "Počet") +
      theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5))
  }
)
#-------------------------------------------------------------------------------
# Graph 1.5 - transplant type
output$typ_transplantace_input_panel <- renderUI(
  wellPanel(
    sliderInput("typ_transplantace_input_years", "Roky:",
                min = unique_transplant_years_decreasing[length(unique_transplant_years_decreasing)],
                max = unique_transplant_years_decreasing[1],
                value = c(unique_transplant_years_decreasing[1] - 5,
                          unique_transplant_years_decreasing[1]),
                step = 1,
                sep = '',
                width = '100%'),
    class = 'vertical-layout-well-panel-input'
  )
)
output$typ_transplantace <- renderPlot(
  {
    input_year_bot <- input$typ_transplantace_input_years[1]
    input_year_top <- input$typ_transplantace_input_years[2]
    if(any(sapply(list(input_year_bot,
                       input_year_top), is.null)
    ) == TRUE)
      return()
    
    years_seq <- seq(input$typ_transplantace_input_years[1],
                     input$typ_transplantace_input_years[2])
    plot_data <- data.frame(data_tab1 %>%
                              filter(rok_transplantace %in%
                                       years_seq) %>%
                              select(typ_darce, HLA_kod, HLA, typ_stepu, rok_transplantace))
    plot_data$typ_transplantace <- translate_transplant_types(plot_data)
    
    ggplot(
      plot_data,
      aes(typ_transplantace)) +
      geom_bar() +
      coord_flip() +
      labs(x = "", y = "Počet")
  }
)
#-------------------------------------------------------------------------------
# Graph 1.6 - transplant type - evolution in time
output$typ_transplantace_vyvoj_v_case <- renderPlot(
  {
    plot_data <- data.frame(data_tab1 %>%
                              select(typ_darce, HLA_kod, HLA, typ_stepu, rok_transplantace))
    plot_data$typ_transplantace <- translate_transplant_types(plot_data)
    plot_data$rok_transplantace <- as.factor(plot_data$rok_transplantace)
      
    ggplot(plot_data, aes(x = rok_transplantace)) +
      geom_bar(aes(fill = typ_transplantace)) +
      labs(x = "Rok transplantace", y = "Počet") +
      scale_fill_brewer(palette = "Blues", na.value = 'limegreen')
  }
)
#-------------------------------------------------------------------------------
# Graph 1.7 - typ stepu
output$typ_stepu <- renderPlot(
  {
    plot_data <- data.frame(data_tab1 %>%
                              select(typ_stepu))
    ggplot(plot_data, aes(typ_stepu)) +
      geom_bar() +
      coord_flip() +
      labs(x = "", y = "Počet")
  }
)
#-------------------------------------------------------------------------------
# Graph 1.8 - pohlavi pacienta v letech
output$pohlavi_pacienta_v_letech <- renderPlot(
  {
    plot_data <- plot_data <- data.frame(data_tab1 %>%
                                           select(pohlavi, rok_transplantace))
    plot_data$rok_transplantace <- as.factor(plot_data$rok_transplantace)
    
    ggplot(plot_data, aes(x = rok_transplantace)) +
      geom_bar(aes(fill = pohlavi), position = "fill") +
      labs(x = "Rok", y = "%") +
      scale_fill_brewer(palette = "Blues") +
      scale_y_continuous(labels = scales::percent) +
      geom_hline(yintercept = 0.5)
  }
)
#-------------------------------------------------------------------------------
# Graph 1.9 - vek pacientu
output$vek_pacientu <- renderPlot(
  {
    plot_data <- plot_data <- data.frame(data_tab1 %>%
                                           select(vek, rok_transplantace))
    plot_data$rok_transplantace <- as.factor(plot_data$rok_transplantace)
    plot_data$vek <- as.numeric(plot_data$vek)
    
    ggplot(plot_data, aes(x = rok_transplantace, y = vek)) +
      geom_point() +
      scale_y_continuous(breaks = seq(0, 150, by = 5)) +
      stat_summary(fun.y = median, colour = "red", size = 2, geom = "point")
  }
)
#-------------------------------------------------------------------------------
# Graph 1.13 - HCTCI pasma
output$hctci_pasma <- renderPlot(
  {
    plot_data <- plot_data <- data.frame(data_tab1 %>%
                                           select(hctci_score, rok_transplantace))
    plot_data$rok_transplantace <- as.factor(plot_data$rok_transplantace)
    plot_data$hctci_pasmo <- cut(plot_data$hctci_score, c(0,1,3,10), labels = c("0", "1-2", ">2"),include.lowest = T)
      
    ggplot(plot_data, aes(x = rok_transplantace)) +
      geom_bar(aes(fill = hctci_pasmo), position = "fill") +
      labs(x = "Rok", y = "%") +
      scale_fill_brewer(palette = "Blues", na.value = 'limegreen') +
      scale_y_continuous(labels = scales::percent) +
      geom_hline(yintercept = 0.5)
  }
)
#-------------------------------------------------------------------------------
# Graph 1.14 - pripravny rezim v poslednim roce
output$pripravny_rezim_title <- renderUI(
  h2(paste('Přípravný režim (', as.integer(format(Sys.Date(), "%Y")) - 1, ')', sep = ''))
)
output$pripravny_rezim_input_panel <- renderUI(
  wellPanel(
    selectInput("pripravny_rezim_input_year", "Rok:",
                choices = unique_transplant_years_decreasing,
                selected = as.integer(format(Sys.Date(), "%Y")) - 1),
    class = 'vertical-layout-well-panel-input'
  )
)
output$pripravny_rezim <- renderPlot(
  {
    input_year <- input$pripravny_rezim_input_year
    if(is.null(input_year))
      return()
    plot_data <- data.frame(data_tab1 %>% 
                              filter(rok_transplantace == input_year) %>%
                              select(pripravny_rezim))
    ggplot(plot_data, aes(pripravny_rezim)) +
      geom_bar() +
      coord_flip() +
      labs(x = "", y = "Počet")
  }
)
#-------------------------------------------------------------------------------
# Graph 1.15 - vyvoj spektra pripravnych rezimu
output$vyvoj_spektra_pripravnych_rezimu_input_panel <- renderUI(
  wellPanel(
    sliderInput("vyvoj_spektra_pripravnych_rezimu_input_years", "Roky:",
                min = unique_transplant_years_decreasing[length(unique_transplant_years_decreasing)],
                max = unique_transplant_years_decreasing[1],
                value = c(unique_transplant_years_decreasing[1] - 5,
                          unique_transplant_years_decreasing[1]),
                step = 1,
                sep = '',
                width = '100%'),
    class = 'vertical-layout-well-panel-input'
  )
)
output$vyvoj_spektra_pripravnych_rezimu <- renderPlot(
  {
    input_year_bot <- input$vyvoj_spektra_pripravnych_rezimu_input_years[1]
    input_year_top <- input$vyvoj_spektra_pripravnych_rezimu_input_years[2]
    if(any(sapply(list(input_year_bot,
                       input_year_top), is.null)
    ) == TRUE)
      return()
    
    years_seq <- seq(input$vyvoj_spektra_pripravnych_rezimu_input_years[1],
                     input$vyvoj_spektra_pripravnych_rezimu_input_years[2])
    plot_data <- data.frame(data_tab1 %>%
                              filter(rok_transplantace %in% years_seq) %>%
                              select(pripravny_rezim, rok_transplantace))
    
    pripravny_rezim_len <- length(unique(plot_data$pripravny_rezim))
    # colourRects <- data.frame(xstart = seq(0.5,pripravny_rezim_len - 0.5,1), 
    #                           xend = seq(1.5,pripravny_rezim_len + 0.5,1), 
    #                           col = plot_data$pripravny_rezim)
    
    ggplot(data = plot_data) +
      # geom_rect(data = colourRects,
      #           mapping = aes(xmin = xstart ,
      #                         xmax = xend ,
      #                         ymin = -Inf,
      #                         ymax = +Inf ,
      #                         fill = col) ,
      #           alpha = .5,
      #           show.legend = FALSE) +
      geom_bar(aes(pripravny_rezim)) +
      facet_grid(. ~ rok_transplantace) +
      coord_flip() +
      geom_vline(xintercept = seq(0.5,pripravny_rezim_len)) +
      labs(x = '', y = 'Počet') +
      scale_x_discrete() #+
      # theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5))
  }
)
#-------------------------------------------------------------------------------